Soal 1 Membuat Database

create database myshop;

Soal 2 Membuat Table di Dalam Database

Table users
create table users(
    -> id int(8) primary key auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255)
    -> );

Table categories

create table categories(
    -> id int(8) primary key auto_increment,
    -> name varchar(255)
    -> );

Table items

create table items(
    -> id int(8) primary key auto_increment,
    -> name varchar(255),
    -> desciption varchar(255),
    -> price int(8),
    -> stock int(8),
    -> categories_id int(8),
    -> foreign key(categories_id) references categories(id)
    -> );

Soal 3 Memasukkan Data pada Table

Table users
insert into users(name,email,password) values("John Doe", "john@doe.com", "john123"), ("John Doe", "john@doe.com", "john123");

Table Categories
insert into categories(name) values("gadget", "cloth", "men", "women", "branded");

Table Items
insert into items(name,desciption,price,stock,categories_id) values("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1), ("Uniklooh","baju keren dari brand ternama",500000, 50,2), ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);

Soal 4 Mengambil Data pada Table

- Mengambil data kecuali
select id,name,email from users;

- Mengambil data item dengan kondisi >
select * from items where price > 1000000;

- Mengambil data menggunakan Like
 select * from items where name like 'sumsang%';

- Mengambil data di kedua table menggunakan join
select items.name, items.desciption, items.price, items.stock, items.categories_id, categories.name from items inner join categories on items.categories_id = categories.id;

Soal 5 Mengubah Data dari Database
update items set price=2500000 where id=1;